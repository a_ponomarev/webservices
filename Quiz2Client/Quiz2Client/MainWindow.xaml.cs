﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz2Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        localhost.WebService objservice;
        public MainWindow()
        {
            InitializeComponent();
            objservice = new localhost.WebService();
            try
            {
                lvRunners.ItemsSource = objservice.getAllRunnersAsStrings();
            }
            catch (WebException ex)
            {
                MessageBox.Show("Server not running!");
            }

        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string name = tbName.Text;
                int distance = Int32.Parse(tbDistance.Text);
                double secs = Double.Parse(tbSeconds.Text);


                if (name == "")
                {
                    MessageBox.Show("Name shouldn't be empty");
                    return;
                }

                if (distance < 0 || secs < 0)
                {
                    MessageBox.Show("Distance and Seconds should be non-negative");
                    return;
                }


                objservice.RegisterRunner(name, distance, secs);

                tbName.Text = "";
                tbDistance.Text = "";
                tbSeconds.Text = "";

                lvRunners.ItemsSource = objservice.getAllRunnersAsStrings();


            }
            catch (WebException ex)
            {
                MessageBox.Show("Server not running!");
            }
            catch (FormatException ex)
            {

                MessageBox.Show("Distance and Seconds should be numeric");
            }



        }

        private void btnFastest_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string name;
                int distance;
                double secs;

                

                MessageBox.Show((objservice.getFastestRunner(out name, out distance, out secs))? "The fastest is " + name + ". He ran his distance of " + distance + " for " + secs + " secs!" : "No runners in DB!");
            }
            catch (WebException ex)
            {
                MessageBox.Show("Server not running!");
            }
        }
    }
}
