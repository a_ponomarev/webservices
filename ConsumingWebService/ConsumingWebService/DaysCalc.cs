﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsumingWebService
{
    public partial class DaysCalc : Form
    {
        public DaysCalc()
        {
            InitializeComponent();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            localhost.WebService objservice = new localhost.WebService();

            int day, month, year;

            day = Convert.ToInt32(txtDay.Text.ToString());
            month = Convert.ToInt32(txtMonth.Text.ToString());
            year = Convert.ToInt32(txtYear.Text.ToString());

            int days = objservice.converttodaysweb(day, month, year);

            MessageBox.Show("You are " + days + " days old");

        }
    }
}
