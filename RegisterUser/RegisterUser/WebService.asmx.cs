﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

namespace RegisterUser
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {

        public enum Gender { Male, Female, NA };
        public enum Pet { Cat, Dog, Other, NA };
        public enum Continent { Europe, America, Australia, Africa, Asia };

        [WebMethod]
        public string RegisterUser(string name, int age, Gender gender, List<Pet> pets, Continent continent)
        {

            if (name.Length < 2 || name.Length > 50)
            {
                throw new SoapException("Name should be 2-50 chars", SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri);

            }


            string str = "";

            foreach (Pet p in pets)
            {
                str += ((str.Length == 0) ? "" : ",") + p.ToString();
            }


            //using (StreamWriter writetext = new StreamWriter(@"D:\data.txt"))
            //{
            //    writetext.WriteLine(name + ";" + age + ";" + gender + ";" + str + ";" + continent);
            //}
            using (var dbContext = new UserDBContext())
            {
                try
                {
                    var user = new User() { Name = name, Age = age, Gender = gender, Pets = str, Continent = continent };
                    dbContext.Users.Add(user);
                    dbContext.SaveChanges();
                }
                catch (DbUpdateException ex)
                {

                    throw new SoapException("Validation error occured", SoapException.ServerFaultCode, Context.Request.Url.AbsoluteUri, ex);
                }
                    
                
                
            }
            return "Person registered: " + name + ", age: " + age + "; " + gender + "; " + str + "; " + continent;

        }

        [WebMethod]
        public Gender ExposeGender(int num)
        {
            Gender radBtnChecked = Gender.NA;

            switch (num)
            {
                case 1:
                    radBtnChecked = Gender.Male;
                    break;
                case 2:
                    radBtnChecked = Gender.Female;
                    break;
            }

            return radBtnChecked;
        }

        [WebMethod]
        public List<Pet> ExposePet(List<string> checkedPetsString)
        {

            List<Pet> petsList = new List<Pet>();

            foreach (var item in checkedPetsString)
            {
                Pet animal = (Pet)Enum.Parse(typeof(Pet), item);
                petsList.Add(animal);
            }

            if (petsList.Count == 0)
                petsList.Add(Pet.NA);

            return petsList;
        }

        [WebMethod]
        public Continent ExposeContinent(string continentSelected)
        {

            Continent continent = (Continent)Enum.Parse(typeof(Continent), continentSelected);

            return continent;
        }
    }
}
