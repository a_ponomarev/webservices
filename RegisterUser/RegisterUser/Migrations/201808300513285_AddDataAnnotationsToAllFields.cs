namespace RegisterUser.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDataAnnotationsToAllFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Pets", c => c.String(nullable: false, maxLength: 20));
            AddColumn("dbo.Users", "Continent", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Users", "Name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Users", "Gender", c => c.String(nullable: false, maxLength: 10));
            Sql("ALTER TABLE Users WITH NOCHECK ADD CONSTRAINT CK_Gender CHECK (Users.Gender IN ('Male', 'Female', 'NA'))");
            Sql("ALTER TABLE Users WITH NOCHECK ADD CONSTRAINT CK_Continent CHECK (Users.Continent IN ('Europe', 'America', 'Australia', 'Africa', 'Asia'))");
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Gender", c => c.String(nullable: false));
            AlterColumn("dbo.Users", "Name", c => c.String());
            DropColumn("dbo.Users", "Continent");
            DropColumn("dbo.Users", "Pets");
        }
    }
}
