namespace RegisterUser.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GenderToReceiveString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "Gender", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Gender", c => c.Int(nullable: false));
        }
    }
}
