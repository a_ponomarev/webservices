namespace RegisterUser.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GenderRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "Gender", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Gender", c => c.String());
        }
    }
}
