namespace RegisterUser.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FieldsTemporaryComment : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Users", "Pets");
            DropColumn("dbo.Users", "Continent");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "Continent", c => c.String());
            AddColumn("dbo.Users", "Pets", c => c.String());
        }
    }
}
