﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using static RegisterUser.WebService;

namespace RegisterUser
{


    public class User
    {

        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        public int Age { get; set; }

        [Column("Gender")]
        [Required]
        [MaxLength(10)]
        public string GenderString
        {
            get { return Gender.ToString(); }
            private set
            {

                Gender = value.ParseEnum<Gender>();

            }
        }

        [NotMapped]
        public Gender Gender { get; set; }

        [NotMapped]
        public Continent Continent { get; set; }

        [Required]
        [MaxLength(20)]
        public string Pets { get; set; }

        [Column("Continent")]
        [Required]
        [MaxLength(20)]
        public string ContinentString
        {
            get { return Continent.ToString(); }
            private set
            {

                Continent = value.ParseEnum<Continent>();

            }
        }
        //public string Continent { get; set; }
    }

    public static class StringExtensions
    {
        public static T ParseEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }

}