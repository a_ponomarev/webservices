﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Quiz2Server
{
    public class RunnersDBContext : DbContext
    {

        public DbSet<Runner> Runners { get; set; }

    }
}