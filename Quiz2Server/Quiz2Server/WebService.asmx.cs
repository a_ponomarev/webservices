﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

namespace Quiz2Server
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {

        [WebMethod]
        public int RegisterRunner(string name, int distanceMeters, double runtimeSeconds)
        {
            using (var dbcontext = new RunnersDBContext())
            {
                try
                {
                    var runner = new Runner() { Name = name, DistanceMeters = distanceMeters, RuntimeSeconds = runtimeSeconds };
                    dbcontext.Runners.Add(runner);
                    dbcontext.SaveChanges();

                    if (runner == null) {
                        return -1;
                    }

                    return runner.Id;
                }
                catch (DbUpdateException ex)
                {
                    return -1;
                }



            }



        }

        [WebMethod]
        public List<string> getAllRunnersAsStrings()
        {
            using (var dbcontext = new RunnersDBContext())
            {
                List<Runner> runnersObjects = dbcontext.Runners.ToList();

                List<string> runners = new List<string>();

                foreach (var item in runnersObjects)
                {
                    runners.Add(item.ToString());
                }

                return runners;
            }
        }

        [WebMethod]
        public bool getFastestRunner(out string name, out int distanceMeters, out double runtimeSeconds)
        {
            using (var dbcontext = new RunnersDBContext())
            {

                Runner runner = dbcontext.Runners.Where(p => p.RuntimeSeconds == dbcontext.Runners.Min(r => r.RuntimeSeconds)).SingleOrDefault();

                if (runner == null)
                {
                    name = "";
                    distanceMeters = 0;
                    runtimeSeconds = 0;
                    return false;
                }

                name = runner.Name;
                distanceMeters = runner.DistanceMeters;
                runtimeSeconds = runner.RuntimeSeconds;

                

                return true;
            }
        }
    }
}
