﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Quiz2Server
{
    public class Runner
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int DistanceMeters { get; set; }

        [Required]
        [Range(0, double.MaxValue)]
        public double RuntimeSeconds { get; set; }

        public override string ToString()
        {
            return Name + " ran " + DistanceMeters +" meters in " + RuntimeSeconds + " seconds";
        }
    }
}