namespace Quiz2Server.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FloatToDoubleForRuntimeSeconds : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Runners", "RuntimeSeconds", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Runners", "RuntimeSeconds", c => c.Single(nullable: false));
        }
    }
}
