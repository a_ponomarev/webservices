﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Services.Protocols;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace RegisterPeople
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        localhost.WebService objservice;

        public MainWindow()
        {
            InitializeComponent();

            objservice = new localhost.WebService();
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //TextBox txtName = (TextBox)this.FindName("txtName");
            //TextBox txtAge = (TextBox)this.FindName("txtAge");

            

            string error = "";
            string strName = tbName.Text;
            string strAge = tbAge.Text;

            //if (strName.Length < 2 || strName.Length > 50)
            //{
            //    error += "Name should be 2-50 chars\n";
            //}

            int age;

            if (int.TryParse(strAge, out age) != true)
            {
                error += "Age must be a number 1-150\n";
            }
            else
            {
                if (age < 1 || age > 150)
                {
                    error += "Age must be a number 1-150\n";
                }
            }

            // TODO: get value selected in radio buttons (gender)

            var checkedGenreButton = mainContainer.Children.OfType<RadioButton>()
                                      .FirstOrDefault(r => r.IsChecked.HasValue && r.IsChecked.Value);


            int checkedGenre = (checkedGenreButton.Content.ToString() == "M") ? 1 : (checkedGenreButton.Content.ToString() == "F") ? 2 : 0;


            // TODO: get value selected in check boxes (pets)

            var checkedPets = mainContainer.Children.OfType<CheckBox>().Where(cb => cb.IsChecked.HasValue && cb.IsChecked.Value).ToList();

            List<string> checkedPetsString = new List<string>();

            foreach (CheckBox cb in checkedPets)
            {
                checkedPetsString.Add(cb.Content.ToString());
            }

            string[] myArray = checkedPetsString.ToArray();

            // TODO: get value selected in combo box (continent)

            string continentSelected = cbxContinents.SelectionBoxItem.ToString();

            if (error.Length == 0)
            {
                try
                {
                    MessageBox.Show(objservice.RegisterUser(strName, age, objservice.ExposeGender(checkedGenre), objservice.ExposePet(myArray), objservice.ExposeContinent(continentSelected)));

                }catch (SoapException ex)
                {
                    
                       
                    MessageBox.Show(ex.Message);
                    
                }catch (WebException)
                {


                    MessageBox.Show("The server is down. Unable to register an entry!");

                }



            }
            else
            {
                MessageBox.Show("Data is invalid:\n" + error);
            }




        }
    }
}
