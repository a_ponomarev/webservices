﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ConsumingWebServiceInWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            localhost.WebService objservice = new localhost.WebService();

            int day, Month, Year, Days;

            TextBox txtDay = (TextBox)this.FindName("txtDay");
            TextBox txtMonth = (TextBox)this.FindName("txtMonth");
            TextBox txtYear = (TextBox)this.FindName("txtYear");

            day = Convert.ToInt32(txtDay.Text);
            Month = Convert.ToInt32(txtMonth.Text);
            Year = Convert.ToInt32(txtYear.Text);
            Days = objservice.converttodaysweb(day, Month, Year);
            MessageBox.Show("You are   " + Days + "   days old");
        }
    }
}
